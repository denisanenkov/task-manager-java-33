# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

- NAME: Anenkov Denis

- E-mail: denk.an@inbox.ru

- Second E-mail: denk.an@bk.ru

# SOFTWARE

JDK version: 1.8

OS: MS Windows 10

# TECHNOLOGY STACK

- JAVA

- MAVEN

# MAVEN PROJECT BUILD

```bash
cd project_directory

mvn clean

mvn install
```

# PROJECT RUN

```bash
java -jar .\task-manager-java-33\target\task-manager-1.0.0.jar
```
