package ru.anenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.TaskDTO;
import ru.anenkov.tm.endpoint.TaskEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.exception.system.IncorrectDataException;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class TaskFindByIndexClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Show-task-by-index";
    }

    @Override
    public @Nullable String description() {
        return "Show task by index";
    }

    @Async
    @Override
    @EventListener(condition = "@taskFindByIndexClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[SHOW TASK]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final TaskDTO task = taskEndpoint.findOneByIndex(bootstrap.getSession(), index);
        if (task == null) throw new IncorrectDataException();
        System.out.println("" +
                "NAME: " + task.getName() +
                "DESCRIPTION: " + task.getName() +
                ", \nUSER ID: " + task.getUserId()
        );
        System.out.println("[SUCCESS]");
    }

}
