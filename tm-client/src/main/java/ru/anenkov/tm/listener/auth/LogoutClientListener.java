package ru.anenkov.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.SessionEndpoint;
import ru.anenkov.tm.enumeration.Role;

@Component
public class LogoutClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Override
    public @Nullable String arg() {
        return "-lout";
    }

    @Override
    public @Nullable String command() {
        return "Logout";
    }

    @Override
    public @Nullable String description() {
        return "Logout user";
    }

    @Async
    @Override
    @EventListener(condition = "@logoutClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[LOGOUT]");
        sessionEndpoint.closeSession(bootstrap.getSession());
        bootstrap.clearSession();
        System.out.println("[LOGOUT SUCCESS]");
    }
 
}
