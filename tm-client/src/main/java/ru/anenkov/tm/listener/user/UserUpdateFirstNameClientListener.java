package ru.anenkov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.UserEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class UserUpdateFirstNameClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String command() {
        return "Update-first-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Update first name";
    }

    @Async
    @Override
    @EventListener(condition = "@userUpdateFirstNameClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[UPDATE USER FIRST NAME]");
        System.out.print("ENTER NEW USER FIRST NAME: ");
        @NotNull final String newFirstName = TerminalUtil.nextLine();
        userEndpoint.updateUserFirstName(bootstrap.getSession(), newFirstName);
        System.out.println("[NAME UPDATE SUCCESS]");
    }

}
