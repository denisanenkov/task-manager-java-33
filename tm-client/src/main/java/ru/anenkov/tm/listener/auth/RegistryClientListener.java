package ru.anenkov.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.UserEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class RegistryClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @Override
    public @Nullable String arg() {
        return "-reg";
    }

    @Override
    public @Nullable String command() {
        return "Registry";
    }

    @Override
    public @Nullable String description() {
        return "Registry user";
    }

    @Async
    @Override
    @EventListener(condition = "@registryClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[REGISTRATION]");
        System.out.print("ENTER LOGIN: ");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @NotNull final String password = TerminalUtil.nextLine();
        userEndpoint.createUser(bootstrap.getSession(), login, password);
        System.out.println("[SUCCESS]");
    }
 
}
