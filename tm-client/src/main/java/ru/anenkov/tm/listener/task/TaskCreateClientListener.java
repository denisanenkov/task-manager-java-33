package ru.anenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.TaskEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class TaskCreateClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Task-create";
    }

    @Override
    public @Nullable String description() {
        return "Create task";
    }

    @Async
    @Override
    @EventListener(condition = "@taskCreateClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.print("ENTER TASK NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER TASK DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        taskEndpoint.createWithDescription(bootstrap.getSession(), name, description);
        System.out.println("[CREATE SUCCESS]");
    }

}
