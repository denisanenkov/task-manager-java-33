package ru.anenkov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.UserDTO;
import ru.anenkov.tm.endpoint.UserEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class ProfileClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Profile";
    }

    @Override
    public @Nullable String description() {
        return "Show user profile";
    }

    @Async
    @Override
    @EventListener(condition = "@profileClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[USER PROFILE]");
        System.out.print("ENTER LOGIN OF USER: ");
        String login = TerminalUtil.nextLine();
        UserDTO user = userEndpoint.findByLoginUser(bootstrap.getSession(), login);
        System.out.println("LOGIN: " + user.getLogin() +
                ", \nFIRST NAME: " + user.getFirstName() +
                ", \nMIDDLE NAME: " + user.getMiddleName() +
                ", \nLAST NAME: " + user.getLastName() +
                ", \nEMAIL: " + user.getEmail());
        System.out.println("[SUCCESS]");
    }

}
