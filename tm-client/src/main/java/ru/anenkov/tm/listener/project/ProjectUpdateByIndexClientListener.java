package ru.anenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.ProjectEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class ProjectUpdateByIndexClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Project-update-by-index";
    }

    @Override
    public @Nullable String description() {
        return "Update project - list by index";
    }

    @Async
    @Override
    @EventListener(condition = "@projectUpdateByIndexClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[UPDATE PROJECT]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.print("ENTER NEW NAME PROJECT: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION PROJECT: ");
        @NotNull final String description = TerminalUtil.nextLine();
        projectEndpoint.updateByIndexProject(bootstrap.getSession(), index, name, description);
        System.out.println("[UPDATE SUCCESS]");
    }

}

