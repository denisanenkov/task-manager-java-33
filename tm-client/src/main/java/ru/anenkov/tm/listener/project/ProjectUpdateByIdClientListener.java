package ru.anenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.ProjectEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class ProjectUpdateByIdClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Project-update-by-id";
    }

    @Override
    public @Nullable String description() {
        return "Update project - list by id";
    }

    @Async
    @Override
    @EventListener(condition = "@projectUpdateByIdClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[UPDATE PROJECT]");
        System.out.print("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NEW NAME PROJECT: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION PROJECT: ");
        @NotNull final String description = TerminalUtil.nextLine();
        projectEndpoint.updateByIdProject(bootstrap.getSession(), id, name, description);
        System.out.println("[UPDATE SUCCESS]");
    }

}