package ru.anenkov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.ProjectEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class ProjectCreateClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Project-create";
    }

    @Override
    public @Nullable String description() {
        return "Create project";
    }

    @Async
    @Override
    @EventListener(condition = "@projectCreateClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.print("ENTER PROJECT NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER PROJECT DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        projectEndpoint.createProject(bootstrap.getSession(), name, description);
        System.out.println("[CREATE SUCCESS]");
    }

}
