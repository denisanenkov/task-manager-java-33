package ru.anenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.TaskEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class TaskUpdateByIndexClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Task-update-by-index";
    }

    @Override
    public @Nullable String description() {
        return "Update task - list by index";
    }

    @Async
    @Override
    @EventListener(condition = "@taskUpdateByIndexClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[UPDATE TASK]");
        System.out.print("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.print("ENTER NEW NAME TASK: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION TASK: ");
        @NotNull final String description = TerminalUtil.nextLine();
        taskEndpoint.updateTaskByIndex(bootstrap.getSession(), index, name, description);
        System.out.println("[UPDATE SUCCESS]");
    }

}
