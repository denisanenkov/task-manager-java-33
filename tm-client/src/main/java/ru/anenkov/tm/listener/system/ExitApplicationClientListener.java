package ru.anenkov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;

@Component
public class ExitApplicationClientListener extends AbstractListenerClient {

    @Override
    public @Nullable String arg() {
        return "-e";
    }

    @Override
    public @Nullable String command() {
        return "Exit";
    }

    @Override
    public @Nullable String description() {
        return "Exit application";
    }

    @Async
    @Override
    @EventListener(condition = "@exitApplicationClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[SYSTEM EXIT]");
        System.exit(0);
    }

}
