package ru.anenkov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.UserEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class UserUpdateMiddleNameClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String command() {
        return "Update-middle-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Update middle name";
    }

    @Async
    @Override
    @EventListener(condition = "@userUpdateMiddleNameClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[UPDATE USER MIDDLE NAME]");
        System.out.print("ENTER NEW USER MIDDLE NAME: ");
        @NotNull final String newSecondName = TerminalUtil.nextLine();
        userEndpoint.updateUserMiddleName(bootstrap.getSession(), newSecondName);
        System.out.println("[NAME UPDATE SUCCESS]");
    }

}
