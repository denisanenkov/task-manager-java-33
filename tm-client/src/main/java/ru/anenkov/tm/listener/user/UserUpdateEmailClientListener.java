package ru.anenkov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.UserEndpoint;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

@Component
public class UserUpdateEmailClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private UserEndpoint userEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Update-email";
    }

    @Override
    public @Nullable String description() {
        return "Update User Email";
    }

    @Async
    @Override
    @EventListener(condition = "@userUpdateEmailClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[UPDATE USER MAIL]");
        System.out.print("ENTER NEW USER MAIL: ");
        @NotNull final String newUserEmail = TerminalUtil.nextLine();
        userEndpoint.updateUserEmail(bootstrap.getSession(), newUserEmail);
        System.out.println("[OK]");
    }

}
