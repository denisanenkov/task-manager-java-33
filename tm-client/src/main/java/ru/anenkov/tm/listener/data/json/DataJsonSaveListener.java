package ru.anenkov.tm.listener.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.AdminEndpoint;
import ru.anenkov.tm.enumeration.Role;

import java.io.IOException;

@Component
public class DataJsonSaveListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String command() {
        return "Data-json-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from json file";
    }

    @Async
    @Override
    @EventListener(condition = "@dataJsonSaveListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[DATA JSON SAVE]");
        adminEndpoint.saveDataJson(bootstrap.getSession());
        System.out.println("[OK]");
    }

}
