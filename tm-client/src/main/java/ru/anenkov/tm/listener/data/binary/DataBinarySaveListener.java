package ru.anenkov.tm.listener.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.AdminEndpoint;
import ru.anenkov.tm.enumeration.Role;

import java.io.IOException;

@Component
public class DataBinarySaveListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String command() {
        return "Data-bin-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data";
    }

    @Async
    @Override
    @EventListener(condition = "@dataBinarySaveListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[DATA BINARY SAVE]");
        adminEndpoint.saveDataBinary(bootstrap.getSession());
        System.out.println("[OK]");
    }

}
