package ru.anenkov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.anenkov.tm.event.ConsoleEvent;
import ru.anenkov.tm.listener.AbstractListenerClient;
import ru.anenkov.tm.endpoint.TaskDTO;
import ru.anenkov.tm.endpoint.TaskEndpoint;
import ru.anenkov.tm.enumeration.Role;

import java.util.List;

@Component
public class TaskListShowClientListener extends AbstractListenerClient {

    @NotNull
    @Autowired
    private TaskEndpoint taskEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String command() {
        return "Task-list";
    }

    @Override
    public @Nullable String description() {
        return "Get task list";
    }

    @Async
    @Override
    @EventListener(condition = "@taskListShowClientListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) throws Exception {
        System.out.println("[TASK LIST]\n");
        int count = 1;
        @Nullable final List<TaskDTO> taskList = taskEndpoint.findAll(bootstrap.getSession());
        if (taskList.isEmpty()) System.out.println("[TASK LIST IS EMPTY]\n");
        else {
            for (TaskDTO task : taskList) {
                System.out.println("Number of task: " + (count++) + "\nName task: " + task.getName() + "\n");
            }
        }
        System.out.println("[SUCCESS]");
    }

}
