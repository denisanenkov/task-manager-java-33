package ru.anenkov.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import ru.anenkov.tm.bootstrap.BootstrapClient;
import ru.anenkov.tm.endpoint.*;

import java.util.concurrent.Executor;

@Configuration
@ComponentScan("ru.anenkov.tm")
public class AppConfiguration {

    @Bean
    public Executor taskExecutor() {
        final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("Custom-");
        executor.initialize();
        return executor;
    }

    @NotNull
    @Bean
    public AdminUserEndpointService adminUserEndpointService() {
        return new AdminUserEndpointService();
    }

    @NotNull
    @Bean
    public AdminUserEndpoint adminUserEndpoint(
            @NotNull @Autowired final AdminUserEndpointService adminUserEndpointService
    ) {
        return adminUserEndpointService.getAdminUserEndpointPort();
    }

    @NotNull
    @Bean
    public AdminEndpointService adminEndpointService() {
        return new AdminEndpointService();
    }

    @NotNull
    @Bean
    public AdminEndpoint adminEndpoint(
            @NotNull @Autowired final AdminEndpointService adminEndpointService
    ) {
        return adminEndpointService.getAdminEndpointPort();
    }

    @NotNull
    @Bean
    public SessionEndpointService sessionEndpointService() {
        return new SessionEndpointService();
    }

    @NotNull
    @Bean
    public SessionEndpoint sessionEndpoint(
            @NotNull @Autowired final SessionEndpointService sessionEndpointService
    ) {
        return sessionEndpointService.getSessionEndpointPort();
    }

    @NotNull
    @Bean
    public ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @NotNull
    @Bean
    public ProjectEndpoint projectEndpoint(
            @NotNull @Autowired final ProjectEndpointService projectEndpointService
    ) {
        return projectEndpointService.getProjectEndpointPort();
    }

    @NotNull
    @Bean
    public TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @NotNull
    @Bean
    public TaskEndpoint taskEndpoint(
            @NotNull @Autowired final TaskEndpointService taskEndpointService
    ) {
        return taskEndpointService.getTaskEndpointPort();
    }

    @NotNull
    @Bean
    public UserEndpointService userEndpointService() {
        return new UserEndpointService();
    }

    @NotNull
    @Bean
    public UserEndpoint userEndpoint(
            @NotNull @Autowired final UserEndpointService userEndpointService
    ) {
        return userEndpointService.getUserEndpointPort();
    }

}
