package ru.anenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.entitiesDTO.UserDTO;
import ru.anenkov.tm.entity.User;

import java.util.Collection;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    List<User> findAll();

    @Nullable
    User add(@NotNull User user);

    @Nullable
    UserDTO findByIdDTO(@NotNull String id);

    @Nullable
    UserDTO findByLoginDTO(@NotNull String login);

    @Nullable
    UserDTO findByEmailDTO(@NotNull String email);

    @Nullable
    User findByIdEntity(@NotNull String id);

    @Nullable
    User findByLoginEntity(@NotNull String login);

    @Nullable
    User findByEmailEntity(@NotNull String email);

    void removeByLogin(@NotNull String login);

    void removeById(@NotNull String id);

    void removeByEmail(@NotNull String email);

    void clear();

    List<User> merge(@NotNull Collection<User> users);

    User merge(@NotNull User user);

    List<User> merge(@NotNull User... users);

    void load(@Nullable Collection<User> users);

    void load(@NotNull User... users);

    void load(@NotNull User user);

    @Nullable
    List<User> getListEntities();

    List<UserDTO> getListDTOs();

    void createUser(@NotNull String userId, @NotNull String name);

    @NotNull
    Long count();

    void clearEntities();

}
