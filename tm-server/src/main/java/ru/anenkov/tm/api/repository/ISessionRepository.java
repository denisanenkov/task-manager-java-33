package ru.anenkov.tm.api.repository;

import ru.anenkov.tm.dto.entitiesDTO.ProjectDTO;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.Session;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @Nullable
    List<Session> getListEntities();

    @Nullable
    List<Session> getListEntitiesById(@NotNull String userId);

    @Nullable
    List<SessionDTO> getListDTOs();

    void removeByUserId(@NotNull String userId);

    void removeSession(@NotNull Session session);

    @Nullable
    List<Session> findByUserId(@NotNull String userId);

    @Nullable
    Session findSessionById(@NotNull String id);

    boolean isExists(String id);

    void clearAll(@Nullable final String userId);

    void clearAllSessions();

}
