package ru.anenkov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.entitiesDTO.TaskDTO;
import ru.anenkov.tm.dto.entitiesDTO.UserDTO;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.entity.User;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @Nullable
    List<Task> getListEntities();

    List<TaskDTO> getListDTOs();

    @Nullable
    Long count();

    @NotNull
    Task create(@NotNull String userId, @NotNull String name);

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    void add(@NotNull String userId, @NotNull TaskDTO task);

    @NotNull
    void remove(@NotNull String userId, @NotNull Task task);

    @Nullable
    List<TaskDTO> findAll(@NotNull String userId);

    List<Task> findAllEntities(@NotNull String userId);

    @NotNull
    void clear(@NotNull String userId);

    @Nullable
    Task findOneByIndexEntity(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task findOneByNameEntity(@NotNull String userId, @NotNull String name);

    @Nullable
    Task findOneByIdEntity(@NotNull String userId, @NotNull String id);

    @Nullable
    TaskDTO findOneByIndexDTO(@NotNull String userId, @NotNull Integer index);

    @Nullable
    TaskDTO findOneByNameDTO(@NotNull String userId, @NotNull String name);

    @Nullable
    TaskDTO findOneByIdDTO(@NotNull String userId, @NotNull String id);

    @NotNull
    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    void removeOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    void removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task updateOneById(@NotNull String userId, @NotNull String id, @NotNull String newName, @NotNull String newDescription);

    @NotNull
    Task updateOneByName(@NotNull String userId, @NotNull String oldName, @NotNull String newName, @NotNull String newDescription);

    @NotNull
    Task updateOneByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String newName, @NotNull String newDescription);

    @NotNull
    List<Task> merge(@NotNull Collection<Task> tasks);

    @NotNull
    Task merge(@NotNull Task task);

    @NotNull
    List<Task> merge(@NotNull Task... tasks);

    @NotNull
    void clear();

    @NotNull
    void load(@NotNull Collection<Task> tasks);

    @NotNull
    void load(@NotNull Task... tasks);

    @NotNull
    void load(@NotNull Task task);

}
