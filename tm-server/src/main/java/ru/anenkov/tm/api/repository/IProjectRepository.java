package ru.anenkov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.entitiesDTO.ProjectDTO;
import ru.anenkov.tm.dto.entitiesDTO.TaskDTO;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.Task;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    @Nullable
    List<Project> getListEntities();

    @Nullable
    List<ProjectDTO> getListDTOs();

    @Nullable
    Long count();

    @NotNull
    Project create(@NotNull String userId, @NotNull String name);

    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    void add(@NotNull String userId, @NotNull Project project);

    @NotNull
    void remove(@NotNull Project project);

    @Nullable
    List<Project> findAll(@NotNull String userId);

    @NotNull
    void clear(@NotNull String userId);

    @Nullable
    Project findOneByIndexEntity(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findOneByNameEntity(@NotNull String userId, @NotNull String name);

    @Nullable
    Project findOneByIdEntity(@NotNull String userId, @NotNull String id);

    @Nullable
    ProjectDTO findOneByIndexDTO(@NotNull String userId, @NotNull Integer index);

    @Nullable
    ProjectDTO findOneByNameDTO(@NotNull String userId, @NotNull String name);

    @Nullable
    ProjectDTO findOneByIdDTO(@NotNull String userId, @NotNull String id);

    @NotNull
    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    void removeOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    void removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    void updateOneById(@NotNull String userId, @NotNull String id, @NotNull String newName, @NotNull String newDescription);

    @NotNull
    void updateOneByName(@NotNull String userId, @NotNull String oldName, @NotNull String newName, @NotNull String newDescription);

    @NotNull
    List<Project> merge(@NotNull Collection<Project> projects);

    @NotNull
    Project merge(@NotNull Project project);

    @NotNull
    List<Project> merge(@NotNull Project... projects);

    @NotNull
    void clear();

    @NotNull
    void load(@NotNull Collection<Project> projects);

    @NotNull
    void load(@NotNull Project... projects);

    @NotNull
    void load(@NotNull Project project);

    @Nullable
    List<Project> getList();

}
