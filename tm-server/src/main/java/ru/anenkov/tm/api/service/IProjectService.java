package ru.anenkov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.entitiesDTO.ProjectDTO;
import ru.anenkov.tm.dto.entitiesDTO.TaskDTO;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.Task;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface IProjectService extends IService<Project> {

    Project toProject(@Nullable final String userId, @Nullable final ProjectDTO projectDTO);

    ProjectDTO toProjectDTO(@Nullable final Project project);

    List<ProjectDTO> toProjectDTOList(@Nullable final List<Project> project);

    @Nullable
    List<Project> findAllEntities(@Nullable String userId);

    @Nullable
    List<ProjectDTO> findAllDTOs(@Nullable String userId);

    void create(@Nullable String userId, @NotNull String name);

    void create(@Nullable String userId, @NotNull String name, @NotNull String description);

    void add(@Nullable String userId, @NotNull Project project);

    void remove(@Nullable String userId, @NotNull Project project);

    void clear(@Nullable String userId);

    @Nullable
    ProjectDTO findOneByIndexDTO(@Nullable String userId, @NotNull Integer index);

    @Nullable
    ProjectDTO findOneByNameDTO(@Nullable String userId, @NotNull String name);

    @Nullable
    ProjectDTO findOneByIdDTO(@Nullable String userId, @NotNull String id);

    @Nullable
    Project findOneByIdEntity(@Nullable String userId, @NotNull String id);

    @Nullable
    Project findOneByIndexEntity(@Nullable String userId, @NotNull Integer index);

    @Nullable
    Project findOneByNameEntity(@Nullable String userId, @NotNull String name);

    void removeOneByIndex(@Nullable String userId, @NotNull Integer index);

    void removeOneByName(@Nullable String userId, @NotNull String name);

    void removeOneById(@Nullable String userId, @NotNull String id);

    void remove(@Nullable String userId, @Nullable ProjectDTO projectDTO);

    @Nullable
    void updateProjectById(@Nullable String userId, @NotNull String id, @NotNull String name, @NotNull String description);

    @NotNull
    void updateProjectByName(@Nullable String userId, @NotNull String oldName, @NotNull String newName, @NotNull String description);

    @NotNull
    void updateProjectByIndex(@Nullable String userId, @NotNull Integer index, @NotNull String newName, @NotNull String description);

    void load(@Nullable List<Project> projects);

    void load(@Nullable Project... projects);

    @Nullable
    List<Project> getList();

}