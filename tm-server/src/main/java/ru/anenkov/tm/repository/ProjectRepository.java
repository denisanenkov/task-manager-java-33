package ru.anenkov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.anenkov.tm.api.repository.IProjectRepository;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.exception.empty.*;
import ru.anenkov.tm.exception.system.IncorrectDataException;
import ru.anenkov.tm.exception.system.IncorrectIndexException;
import ru.anenkov.tm.dto.entitiesDTO.ProjectDTO;

import java.util.List;

@Repository
@Scope("prototype")
public final class ProjectRepository extends AbstractRepository<Project, ProjectDTO> implements IProjectRepository {

    @Override
    @Nullable
    @SneakyThrows
    public List<Project> getListEntities() {
        return entityManager.createQuery("SELECT e FROM Project e", Project.class)
                .getResultList();
    }

    @Override
    @SneakyThrows
    public List<ProjectDTO> getListDTOs() {
        return entityManager.createQuery("SELECT e FROM ProjectDTO e", ProjectDTO.class)
                .getResultList();
    }

    @Override
    @SneakyThrows
    public @Nullable Long count() {
        return entityManager.createQuery("SELECT COUNT(e) FROM Project e", Long.class)
                .getSingleResult();
    }

    @Override
    @SneakyThrows
    public Project create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        @NotNull final User user = entityManager.find(User.class, userId);
        @NotNull final Project project = new Project(name, user);
        return merge(project);
    }

    @Override
    @SneakyThrows
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        @NotNull final User user = entityManager.find(User.class, userId);
        @NotNull final Project project = new Project(name, description, user);
        return merge(project);
    }

    @Override
    @SneakyThrows
    public void add(
            @NotNull final String userId,
            @Nullable Project project
    ) {
        @NotNull final User user = entityManager.find(User.class, userId);
        project.setUser(user);
        entityManager.persist(project);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(
            @NotNull final String userId
    ) {
        @Nullable final List<Project> listProjects = entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
        return listProjects;
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final List<Project> listProjects = findAll(userId);
        for (@NotNull final Project project : listProjects) {
            entityManager.remove(project);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findOneByIndexEntity(
            @NotNull final String userId,
            @Nullable final Integer index
    ) {
        @Nullable final List<Project> listProjects = entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
        if (listProjects.size() < index) throw new IncorrectIndexException();
        return listProjects.get(index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByNameEntity(
            @NotNull final String userId,
            @Nullable final String name
    ) {
        @Nullable final List<Project> listProjects = entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId AND e.name = :name", Project.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1)
                .getResultList();
        return getFirstEntity(listProjects);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIdEntity(
            @NotNull final String userId,
            @Nullable final String id
    ) {
        @Nullable final List<Project> listProjects = entityManager.createQuery("SELECT e FROM Project e WHERE e.user.id = :userId AND e.id = :id", Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        return getFirstEntity(listProjects);
    }

    @Override
    @SneakyThrows
    public @Nullable ProjectDTO findOneByIndexDTO(String userId, Integer index) {
        @Nullable final List<ProjectDTO> listProjects = entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
        if (listProjects.size() < index) throw new IncorrectIndexException();
        if (listProjects.isEmpty()) throw new IncorrectDataException();
        return listProjects.get(index);
    }

    @Override
    @SneakyThrows
    public @Nullable ProjectDTO findOneByNameDTO(String userId, String name) {
        @Nullable final List<ProjectDTO> listProjects = entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId AND e.name = :name", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1)
                .getResultList();
        return getFirstDTO(listProjects);
    }

    @Override
    @SneakyThrows
    public @Nullable ProjectDTO findOneByIdDTO(String userId, String id) {
        @Nullable final List<ProjectDTO> listProjects = entityManager.createQuery("SELECT e FROM ProjectDTO e WHERE e.userId = :userId AND e.id = :id", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        return getFirstDTO(listProjects);
    }

    @Override
    @SneakyThrows
    public void updateOneById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String newName,
            @NotNull final String newDescription
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable Project project = findOneByIdEntity(userId, id);
        project.setName(newName);
        project.setDescription(newDescription);
        entityManager.merge(project);
    }

    @Override
    @SneakyThrows
    public void updateOneByName(
            @NotNull final String userId,
            @NotNull final String oldName,
            @NotNull final String newName,
            @NotNull final String newDescription
    ) {
        @Nullable Project project = findOneByNameEntity(userId, oldName);
        project.setName(newName);
        project.setDescription(newDescription);
        entityManager.merge(project);
    }

    @Override
    @SneakyThrows
    public void remove(
            @Nullable final Project project
    ) {
        if (project == null) throw new EmptyEntityException();
        entityManager.remove(project);
    }

    @Override
    @SneakyThrows
    public void removeOneByIndex(
            @NotNull final String userId,
            @Nullable final Integer index
    ) {
        if (index == null || index < 0) throw new EmptyIdException();
        @Nullable final Project project = findOneByIndexEntity(userId, index);
        if (project == null) throw new EmptyEntityException();
        entityManager.remove(project);
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeOneByName(
            @NotNull final String userId,
            @Nullable final String name
    ) {
        if (name == null || name.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = findOneByNameEntity(userId, name);
        if (project == null) throw new EmptyEntityException();
        entityManager.remove(project);
    }

    @Override
    @SneakyThrows
    public void removeOneById(
            @NotNull final String userId,
            @Nullable final String id
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = findOneByIdEntity(userId, id);
        if (project == null) throw new EmptyEntityException();
        entityManager.remove(project);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final List<Project> listProjects = getListEntities();
        for (@NotNull final Project project : listProjects) {
            entityManager.remove(project);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> getList() {
        return getListEntities();
    }

}
