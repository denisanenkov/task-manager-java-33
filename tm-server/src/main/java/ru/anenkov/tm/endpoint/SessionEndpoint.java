package ru.anenkov.tm.endpoint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.anenkov.tm.api.endpoint.ISessionEndpoint;
import ru.anenkov.tm.dto.result.Fail;
import ru.anenkov.tm.dto.result.Result;
import ru.anenkov.tm.dto.result.Success;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.service.SessionService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@JsonIgnoreProperties
@Controller
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @Nullable
    @Autowired
    private SessionService sessionService;

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public SessionDTO openSession(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    ) {
        return sessionService.toSessionDTO(sessionService.open(login, password));
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public Result closeSession(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        try {
            sessionService.close(session);
            return new Success();
        } catch (final RuntimeException e) {
            return new Fail();
        }
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public Result closeSessionAll(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        try {
            sessionService.closeAll(session);
            return new Success();
        } catch (final RuntimeException e) {
            return new Fail();
        }
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public List<SessionDTO> allSessions(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        List<SessionDTO> sessions = sessionService.getListSession(session);
        System.out.println(sessions);
        return sessions;
    }

    @WebMethod
    @Override
    @SneakyThrows
    public void clearAllSessions(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "sessionId", partName = "sessionId") final String sessionId
    ) {
        sessionService.validate(session);
        sessionService.closeAll(sessionId);
    }

    @WebMethod
    @Override
    @SneakyThrows
    public SessionDTO findSessionById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "sessionId", partName = "sessionId") final String sessionId
    ) {
        sessionService.validate(session);
        SessionDTO sessionDTO = sessionService.toSessionDTO(sessionService.findSessionById(sessionId));
        return sessionDTO;
    }

}
