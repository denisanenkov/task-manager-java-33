package ru.anenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.anenkov.tm.api.endpoint.ITaskEndpoint;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.dto.entitiesDTO.TaskDTO;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.service.SessionService;
import ru.anenkov.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @Nullable
    @Autowired
    private SessionService sessionService;

    @Nullable
    @Autowired
    private TaskService taskService;

    @WebMethod
    @SneakyThrows
    public void createWithDescription(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        sessionService.validate(session);
        taskService.create(session.getUserId(), name, description);
    }

    @WebMethod
    @SneakyThrows
    public void create(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        taskService.create(session.getUserId(), name);
    }

    @WebMethod
    @SneakyThrows
    public void add(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "task") final TaskDTO task
    ) {
        sessionService.validate(session);
        taskService.add(session.getUserId(), task);
    }

    @WebMethod
    @SneakyThrows
    public void remove(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "task") final TaskDTO task
    ) {
        sessionService.validate(session);
        Task newTask = taskService.toTask(session.getUserId(), task);
        taskService.remove(session.getUserId(), newTask);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public List<TaskDTO> findAll(
            @Nullable @WebParam(name = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        return taskService.toTaskDTOList
                (taskService.getListEntities(session.getUserId()));
    }

    @WebMethod
    @SneakyThrows
    public void clear(
            @Nullable @WebParam(name = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        taskService.clear(session.getUserId());
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public TaskDTO findOneByIndex(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        return taskService.toTaskDTO
                (taskService.findOneByIndexEntity(session.getUserId(), index));
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public TaskDTO findOneByName(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        return taskService.toTaskDTO
                (taskService.findOneByNameEntity(session.getUserId(), name));
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public TaskDTO findOneById(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        return taskService.toTaskDTO
                (taskService.findOneByIdEntity(session.getUserId(), id));
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public void removeOneByIndex(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        taskService.removeOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public void removeOneByName(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        taskService.removeOneByName(session.getUserId(), name);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public void removeOneById(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        taskService.removeOneById(session.getUserId(), id);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public void updateTaskById(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id") final String id,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        sessionService.validate(session);
        taskService.updateTaskById(session.getUserId(), id, name, description);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public void updateTaskByIndex(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id") final Integer index,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        sessionService.validate(session);
        taskService.updateTaskByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    @SneakyThrows
    public void load(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "task") final List<TaskDTO> tasks
    ) {
        sessionService.validate(session);
        taskService.load
                (taskService.toTaskList(session.getUserId(), tasks));
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    public List<TaskDTO> getList(
            @Nullable @WebParam(name = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        return taskService.toTaskDTOList
                (taskService.getListEntities(session.getUserId()));
    }

}
