package ru.anenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.anenkov.tm.api.endpoint.IUserEndpoint;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.dto.entitiesDTO.UserDTO;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.service.SessionService;
import ru.anenkov.tm.service.UserService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@Controller
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @Nullable
    @Autowired
    private SessionService sessionService;

    @Nullable
    @Autowired
    private UserService userService;

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public List<UserDTO> findAllUser(
            @NotNull @WebParam(name = "session") SessionDTO session
    ) {
        sessionService.validate(session);
        return userService.toUserDTOList
                (userService.findAllEntities());
    }

    @WebMethod
    @Override
    @SneakyThrows
    public void createUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password
    ) {
        sessionService.validate(session);
        userService.create(login, password, Role.USER);
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public UserDTO findByIdUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "id") String id
    ) {
        sessionService.validate(session);
        return userService.toUserDTO
                (userService.findByIdEntity(id));
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public UserDTO findByLoginUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "login") String login
    ) {
        sessionService.validate(session);
        return userService.toUserDTO
                (userService.findByLoginEntity(login));
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public UserDTO findByEmailUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @NotNull @WebParam(name = "login") String login
    ) {
        sessionService.validate(session);
        return userService.toUserDTO
                (userService.findByLoginEntity(login));
    }

    @WebMethod
    @Override
    @SneakyThrows
    public void updateUserFirstName(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "newFirstName") String newFirstName
    ) {
        sessionService.validate(session);
        userService.updateUserFirstName(session.getUserId(), newFirstName);
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public void updateUserMiddleName(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "newMiddleName") String newMiddleName
    ) {
        sessionService.validate(session);
        userService.updateUserMiddleName(session.getUserId(), newMiddleName);
    }

    @WebMethod
    @Override
    @SneakyThrows
    public void updateUserLastName(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "newLastName") String newLastName
    ) {
        sessionService.validate(session);
        userService.updateUserLastName(session.getUserId(), newLastName);
    }

    @WebMethod
    @Override
    @SneakyThrows
    public void updateUserEmail(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "newEmail") String newEmail
    ) {
        sessionService.validate(session);
        userService.updateUserEmail(session.getUserId(), newEmail);
    }

    @WebMethod
    @Override
    @SneakyThrows
    public void updatePasswordUser(
            @NotNull @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "newPassword") String newPassword
    ) {
        sessionService.validate(session);
        userService.updatePassword(session.getUserId(), newPassword);
    }

    @WebMethod
    @Override
    @Nullable
    @SneakyThrows
    public List<UserDTO> getListUser(
            @NotNull @WebParam(name = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        return userService.toUserDTOList
                (userService.findAllEntities());
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    public UserDTO findById(
            @NotNull @WebParam(name = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        UserDTO userDTO = userService.toUserDTO
                (userService.findByIdEntity(session.getUserId()));
        return userDTO;
    }

}