package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.api.repository.ITaskRepository;
import ru.anenkov.tm.api.service.ITaskService;
import ru.anenkov.tm.dto.entitiesDTO.ProjectDTO;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.exception.empty.EmptyUserIdException;
import ru.anenkov.tm.exception.system.IncorrectIndexException;
import ru.anenkov.tm.exception.empty.EmptyNameException;
import ru.anenkov.tm.dto.entitiesDTO.TaskDTO;
import ru.anenkov.tm.exception.user.EntityConvertException;
import ru.anenkov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskService extends AbstractService<Task> implements ITaskService {

    @Override
    @SneakyThrows
    public Task toTask(@Nullable final String userId, @Nullable final TaskDTO taskDTO) {
        if (taskDTO == null) throw new EmptyEntityException();
        @Nullable final Task task = findOneByIdEntity(userId, taskDTO.getId());
        if (task == null) throw new EntityConvertException();
        return task;
    }

    @Override
    @SneakyThrows
    public List<Task> toTaskList(
            @Nullable final String userId,
            @Nullable final List<TaskDTO> taskDTOList
    ) {
        if (taskDTOList == null || taskDTOList.isEmpty()) throw new EmptyEntityException();
        @Nullable List<Task> result = new ArrayList<>();
        for (@Nullable final TaskDTO task : taskDTOList) {
            @Nullable Task currentTask = toTask(userId, task);
            result.add(currentTask);
        }
        if (result == null || result.isEmpty()) throw new EntityConvertException();
        return result;
    }

    @Override
    @SneakyThrows
    public TaskDTO toTaskDTO(@Nullable final Task task) {
        @Nullable TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(task.getUser().getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        return taskDTO;
    }

    @Override
    @SneakyThrows
    public List<TaskDTO> toTaskDTOList(@Nullable final List<Task> taskList) {
        @Nullable List<TaskDTO> taskDTOS = new ArrayList<>();
        for (@Nullable final Task taskEntity : taskList) {
            @Nullable TaskDTO taskDTO = new TaskDTO();
            taskDTO.setUserId(taskEntity.getUser().getId());
            taskDTO.setName(taskEntity.getName());
            taskDTO.setDescription(taskEntity.getDescription());
            taskDTOS.add(taskDTO);
        }
        return taskDTOS;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Task> getListEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        @Nullable final List<Task> tasks = taskRepository.findAllEntities(userId);
        return tasks;
    }

    @Override
    @Transactional(readOnly = true)
    public List<TaskDTO> getListDTOs(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        @Nullable final List<TaskDTO> tasks = taskRepository.findAll(userId);
        return tasks;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        taskRepository.create(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        taskRepository.create(userId, name, description);
    }

    @Override
    @Transactional
    public void remove(
            @Nullable String userId,
            @NotNull Task task
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new EmptyEntityException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        taskRepository.removeOneById(userId, task.getId());
    }

    @Override
    @Transactional
    public void add(
            @Nullable String userId,
            @NotNull TaskDTO task
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (task == null) throw new EmptyEntityException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        taskRepository.add(userId, task);
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        @Nullable final List<TaskDTO> tasks = taskRepository.findAll(userId);
        return tasks;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        taskRepository.clear(userId);
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public TaskDTO findOneByIndexDTO(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        @Nullable final TaskDTO task = taskRepository.findOneByIndexDTO(userId, index);
        return task;
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public TaskDTO findOneByNameDTO(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        @Nullable final TaskDTO task = taskRepository.findOneByNameDTO(userId, name);
        return task;
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public TaskDTO findOneByIdDTO(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        @Nullable final TaskDTO task = taskRepository.findOneByIdDTO(userId, id);
        return task;
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public Task findOneByIdEntity(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        @Nullable final Task task = taskRepository.findOneByIdEntity(userId, id);
        return task;
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public Task findOneByIndexEntity(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        @Nullable final Task task = taskRepository.findOneByIndexEntity(userId, index);
        return task;
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public Task findOneByNameEntity(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        @Nullable final Task task = taskRepository.findOneByNameEntity(userId, name);
        return task;
    }


    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        taskRepository.removeOneByIndex(userId, index);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByName(
            final @Nullable String userId,
            final @Nullable String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        taskRepository.removeOneByName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        taskRepository.removeOneById(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @NotNull final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        taskRepository.updateOneById(userId, id, name, description);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateTaskByName(
            @Nullable final String userId,
            @Nullable final String oldName,
            @Nullable final String newName,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (oldName == null || oldName.isEmpty()) throw new IncorrectIndexException();
        if (newName == null || newName.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        taskRepository.updateOneByName(userId, oldName, newName, description);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateTaskByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String newName,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (newName == null || newName.isEmpty()) throw new EmptyNameException();
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        taskRepository.updateOneByIndex(userId, index, newName, description);
    }

    @Override
    @Transactional
    public void load(@Nullable final List<Task> tasks) {
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        taskRepository.load(tasks);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Task> getList() {
        @Nullable final ITaskRepository taskRepository = context.getBean(ITaskRepository.class);
        @Nullable final List<Task> tasks = taskRepository.getList();
        return tasks;
    }

}