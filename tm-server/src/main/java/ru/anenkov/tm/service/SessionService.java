package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.api.repository.ISessionRepository;
import ru.anenkov.tm.api.service.IServiceLocator;
import ru.anenkov.tm.api.service.ISessionService;
import ru.anenkov.tm.constant.DataConst;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.dto.entitiesDTO.UserDTO;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.exception.empty.*;
import ru.anenkov.tm.exception.user.AccessDeniedException;
import ru.anenkov.tm.exception.user.EntityConvertException;
import ru.anenkov.tm.repository.SessionRepository;
import ru.anenkov.tm.util.HashUtil;
import ru.anenkov.tm.util.SignatureUtil;

import java.io.File;
import java.io.FileInputStream;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

@Service
public class SessionService extends AbstractService<Session> implements ISessionService {

    @Autowired
    private UserService userService;

    @Nullable
    @Override
    @SneakyThrows
    public Session toSession(@Nullable final SessionDTO sessionDTO) {
        if (sessionDTO == null) throw new EmptyEntityException();
        @Nullable final Session session = findSessionById(sessionDTO.getId());
        if (session == null) throw new EntityConvertException("session");
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO toSessionDTO(@Nullable final Session session) {
        @Nullable SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(session.getId());
        sessionDTO.setUserId(session.getUser().getId());
        sessionDTO.setTimestamp(session.getTimestamp());
        sessionDTO.setSignature(session.getSignature());
        return sessionDTO;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Session open(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (!checkUserAccess(login, password)) throw new AccessDeniedException();
        @Nullable final User user = userService.findByLoginEntity(login);
        if (user == null) throw new EmptyUserException();
        if (user.isLocked()) throw new AccessDeniedException();
        @Nullable final Session sessionSign = new Session(user);
        sessionSign.setTimestamp(System.currentTimeMillis());
        sessionSign.setSignature(sign(sessionSign));
        @NotNull final ISessionRepository sessionRepository = context.getBean(ISessionRepository.class);
        sessionRepository.merge(sessionSign);
        return sessionSign;
    }

    @Override
    @SneakyThrows
    public boolean checkUserAccess(@NotNull final String login, @NotNull final String password) {
        @Nullable final User user = userService.findByLoginEntity(login);
        if (user == null) return false;
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPassword());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void close(@Nullable final Session session) {
        validate(session);
        @NotNull final ISessionRepository sessionRepository = context.getBean(ISessionRepository.class);
        @Nullable final Session session1 = sessionRepository.findSessionById(session.getId());
        if (session1 != null) sessionRepository.removeSession(session1);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void closeAll(@Nullable final Session session) {
        validate(session);
        @NotNull final ISessionRepository sessionRepository = context.getBean(ISessionRepository.class);
        sessionRepository.clearAll(session.getUser().getId());
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public Session findSessionById(@Nullable final String id) {
        @NotNull final ISessionRepository sessionRepository = context.getBean(ISessionRepository.class);
        return sessionRepository.findSessionById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void close(@Nullable final SessionDTO session) {
        validate(session);
        @NotNull final ISessionRepository sessionRepository = context.getBean(ISessionRepository.class);
        @Nullable final Session session1 = sessionRepository.findSessionById(session.getId());
        if (session1 != null) sessionRepository.removeSession(session1);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void closeAll(@Nullable final SessionDTO session) {
        validate(session);
        @NotNull final ISessionRepository sessionRepository = context.getBean(ISessionRepository.class);
        sessionRepository.clearAll(session.getUserId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void closeAll(@Nullable final String userId) {
        @NotNull final ISessionRepository sessionRepository = context.getBean(ISessionRepository.class);
        sessionRepository.clearAll(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO getUser(@Nullable final SessionDTO session) {
        @Nullable final String userId = getUserId(session);
        return userService.findByIdDTO(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getUserId(@Nullable final SessionDTO session) {
        validate(session);
        return session.getUserId();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<SessionDTO> getListSession(@Nullable final SessionDTO session) {
        validate(session);
        @NotNull final ISessionRepository sessionRepository = context.getBean(ISessionRepository.class);
        @Nullable final List<SessionDTO> list = sessionRepository.getListDTOs();
        return list;
    }

    @Nullable
    @Override
    @SneakyThrows
    public String sign(@Nullable final Session session) {
        @NotNull final Properties properties = new Properties();
        properties.load(new FileInputStream(new File(DataConst.PROPERTIES_PATH)));
        if (session == null) return null;
        session.setSignature("");
        @NotNull final String salt = properties.getProperty("sessions.salt");
        @NotNull final Integer cycle = Integer.parseInt(properties.getProperty("sessions.cycle"));
        @Nullable final String signature = SignatureUtil.sign(SessionDTO.toDTO(session), salt, cycle);
        session.setSignature(signature);
        return session.getSignature();
    }

    @Override
    @SneakyThrows
    public boolean isValid(@Nullable final SessionDTO session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    @SneakyThrows
    @Transactional
    public void validate(@Nullable final SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionDTO temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = temp.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final ISessionRepository sessionRepository = context.getBean(ISessionRepository.class);
        if (!sessionRepository.isExists(session.getId())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUser().getId() == null || session.getUser().getId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = temp.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final ISessionRepository sessionRepository = context.getBean(ISessionRepository.class);
        if (!sessionRepository.isExists(session.getId())) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUser().getId();
        @Nullable final User user = userService.findByIdEntity(userId);
        if (user == null) throw new AccessDeniedException();
        if (!role.equals((user.getRole()))) throw new AccessDeniedException();
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<Session> getList() {
        @NotNull final ISessionRepository sessionRepository = context.getBean(ISessionRepository.class);
        @NotNull final List<Session> sessionList = sessionRepository.getListEntities();
        return sessionList;
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionDTO session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getId();
        @Nullable final UserDTO user = userService.toUserDTO(userService.findByIdEntity(userId));
        if (user == null) throw new AccessDeniedException();
        if (!role.equals((user.getRole()))) throw new AccessDeniedException();
    }

    @SneakyThrows
    @Transactional
    public void clearAll(@Nullable final String userId) {
        @NotNull final ISessionRepository sessionRepository = context.getBean(ISessionRepository.class);
        sessionRepository.clearAll(userId);
    }

}
