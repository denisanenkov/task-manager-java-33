package ru.anenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.anenkov.tm.api.repository.IUserRepository;
import ru.anenkov.tm.api.service.IUserService;
import ru.anenkov.tm.dto.entitiesDTO.ProjectDTO;
import ru.anenkov.tm.dto.entitiesDTO.UserDTO;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.exception.empty.*;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.exception.user.EntityConvertException;
import ru.anenkov.tm.repository.UserRepository;
import ru.anenkov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService extends AbstractService<User> implements IUserService {

    @Override
    @SneakyThrows
    public User toUser(@Nullable final UserDTO userDTO) {
        if (userDTO == null) throw new EmptyEntityException();
        @Nullable User user = findByIdEntity(userDTO.getId());
        if (user == null) throw new EntityConvertException("user");
        return user;
    }

    @Override
    @SneakyThrows
    public UserDTO toUserDTO(@Nullable final User user) {
        @Nullable UserDTO userDTO = new UserDTO();
        if (user == null) throw new EmptyEntityException();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPasswordHash(user.getPasswordHash());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setMiddleName(user.getMiddleName());
        userDTO.setLastName(user.getLastName());
        userDTO.setLocked(user.isLocked());
        userDTO.setEmail(user.getEmail());
        userDTO.setRole(user.getRole());
        return userDTO;
    }

    @Override
    @SneakyThrows
    public List<UserDTO> toUserDTOList(@Nullable final List<User> userList) {
        if (userList == null || userList.isEmpty()) throw new EmptyEntityException();
        @Nullable List<UserDTO> userDTOS = new ArrayList<>();
        for (@Nullable User user : userList) {
            @Nullable UserDTO userDTO = new UserDTO();
            userDTO.setId(user.getId());
            userDTO.setLogin(user.getLogin());
            userDTO.setPasswordHash(user.getPasswordHash());
            userDTO.setFirstName(user.getFirstName());
            userDTO.setMiddleName(user.getMiddleName());
            userDTO.setLastName(user.getLastName());
            userDTO.setLocked(user.isLocked());
            userDTO.setEmail(user.getEmail());
            userDTO.setRole(user.getRole());
            userDTOS.add(userDTO);
        }
        return userDTOS;
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<User> findAllEntities() {
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        @Nullable final List<User> users = userRepository.getListEntities();
        return users;
    }

    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<UserDTO> findAllDTO() {
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        @Nullable final List<UserDTO> users = userRepository.getListDTOs();
        return users;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public UserDTO findByIdDTO(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        @Nullable final UserDTO user = userRepository.findByIdDTO(id);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findByIdEntity(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        @Nullable final User user = userRepository.findByIdEntity(id);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public UserDTO findByLoginDTO(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        @Nullable final UserDTO user = userRepository.findByLoginDTO(login);
        return user;
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findByLoginEntity(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        @Nullable final User user = userRepository.findByLoginEntity(login);
        return user;
    }

    @Nullable
    @SneakyThrows
    @Transactional(readOnly = true)
    public User findByEmailEntity(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        @Nullable final User user = userRepository.findByEmailEntity(email);
        return user;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeUser(@Nullable final User user) {
        if (user == null) throw new EmptyUserException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        userRepository.remove(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyUserException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        userRepository.removeById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        userRepository.removeByEmail(email);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        userRepository.removeByLogin(login);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable User user = new User();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        userRepository.merge(user);
    }


    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        @Nullable User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setEmail(email);
        userRepository.merge(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        @Nullable User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);
        userRepository.merge(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void updateUserFirstName(
            @Nullable final String userId,
            @Nullable final String newFirstName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newFirstName == null || newFirstName.isEmpty()) throw new EmptyPasswordException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        @Nullable User user = findByIdEntity(userId);
        user.setFirstName(newFirstName);
        userRepository.merge(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void updateUserMiddleName(
            @Nullable final String userId,
            @Nullable final String newMiddleName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newMiddleName == null || newMiddleName.isEmpty()) throw new EmptyParameterException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        @Nullable User user = findByIdEntity(userId);
        user.setMiddleName(newMiddleName);
        userRepository.merge(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void updateUserLastName(
            @Nullable final String userId,
            @Nullable final String newLastName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newLastName == null || newLastName.isEmpty()) throw new EmptyParameterException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        @Nullable User user = findByIdEntity(userId);
        user.setLastName(newLastName);
        userRepository.merge(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void updateUserEmail(
            @Nullable final String userId,
            @Nullable final String newEmail
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newEmail == null || newEmail.isEmpty()) throw new EmptyParameterException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        @Nullable User user = findByIdEntity(userId);
        user.setEmail(newEmail);
        userRepository.merge(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public void updatePassword(
            @Nullable final String userId,
            @Nullable final String newPassword
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (newPassword == null || newPassword.isEmpty()) throw new EmptyPasswordException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        @Nullable User user = findByIdEntity(userId);
        user.setPasswordHash(HashUtil.salt(newPassword));
        userRepository.merge(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        @Nullable User user = userRepository.findByLoginEntity(login);
        user.setLocked(true);
        userRepository.merge(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        @Nullable User user = userRepository.findByLoginEntity(login);
        assert user != null;
        user.setLocked(false);
        userRepository.merge(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void deleteUserByLogin(@Nullable final String login) {
        @Nullable final IUserRepository userRepository = context.getBean(IUserRepository.class);
        userRepository.removeByLogin(login);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional(readOnly = true)
    public List<User> getList() {
        return findAllEntities();
    }

}
