package ru.anenkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.anenkov.tm.api.repository.IRepository;
import ru.anenkov.tm.api.service.IService;
import ru.anenkov.tm.dto.entitiesDTO.AbstractEntityDTO;
import ru.anenkov.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @Nullable
    @PersistenceContext
    private EntityManager entityManager;

    @Nullable
    @Autowired
    protected ApplicationContext context;

    public void removeEntity(@NotNull T entity) {
        entityManager.remove(entity);
    }

    public List<T> merge(@Nullable final Collection<T> t) {
        if (t == null || t.isEmpty()) return null;
        @Nullable List<T> entities = new ArrayList<>();
        for (@NotNull final T entity : t) {
            assert entityManager != null;
            entityManager.merge(entity);
            entities.add(entity);
        }
        return entities;
    }

    public T merge(@Nullable final T t) {
        if (t == null) return null;
        assert entityManager != null;
        entityManager.merge(t);
        return t;
    }

    public List<T> merge(@Nullable final T... t) {
        if (t == null || t.length == 0) return null;
        @Nullable List<T> entities = new ArrayList<>();
        for (@Nullable final T entity : t) {
            entityManager.merge(entity);
            entities.add(entity);
        }
        return entities;
    }

    public void load(@Nullable final Collection<T> t) {
        clear();
        entityManager.merge(t);
    }

    public void load(@Nullable final T... t) {
        clear();
        entityManager.merge(t);
    }

    public void load(@Nullable final T t) {
        clear();
        entityManager.merge(t);
    }

    public void clear() {
        entityManager.clear();
    }

    public void remove(@NotNull T entity) {
        entityManager.remove(entity);
    }

}
